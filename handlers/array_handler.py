import datetime
import json
from abc import ABC
from typing import Optional, Awaitable, Any

import tornado.web
from sqlalchemy.future import select

from database.db_connector import DatabaseConnector
from models.array import ArrayModel
from utils.validate_array_data import is_valid_array


class ArrayHandler(tornado.web.RequestHandler, ABC):
    async def prepare(self) -> Optional[Awaitable[None]]:
        self.json = json.loads(self.request.body)
        self.db_session = await DatabaseConnector.get_db_session()

    async def write_error(self, status_code: int, **kwargs: Any) -> None:
        self.set_status(status_code=status_code)
        self.write({'result': 'ERROR'})

    async def post(self):
        if 'array' not in self.json:
            await self.write_error(400)
            return

        if not is_valid_array(self.json['array']):
            await self.write_error(400)
            return

        model_data = {'array': self.json.get('array'),
                      'result_array': sorted(self.json.get('array')),
                      'date_of_creation': datetime.datetime.now()
                      }

        record = ArrayModel(**model_data)

        async with self.db_session() as session:
            async with session.begin():
                session.add(record)
            await session.commit()

        self.write({'result': 'OK', 'id': record.id})
        return

    async def get(self):
        if 'id' not in self.json or type(self.json['id']) is not int:
            await self.write_error(400)
            return

        async with self.db_session() as session:
            async with session.begin():
                result = await session.execute(select(ArrayModel).filter_by(
                    id=self.json['id']))

        resp_data: ArrayModel = result.scalars().first()

        if resp_data is None:
            await self.write_error(400)
            return

        self.write({'result': 'OK', 'id': resp_data.id,
                    'array': resp_data.array,
                    'result_array': resp_data.result_array,
                    'date_of_creation': str(resp_data.date_of_creation)})
        return
