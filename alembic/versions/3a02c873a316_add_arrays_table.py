"""add arrays table

Revision ID: 3a02c873a316
Revises: 
Create Date: 2022-03-06 09:51:08.179805

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3a02c873a316'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('arrays',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('array', sa.JSON(), nullable=True),
    sa.Column('result_array', sa.JSON(), nullable=True),
    sa.Column('date_of_creation', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('arrays')
    # ### end Alembic commands ###
