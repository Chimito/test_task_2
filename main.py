import os

import tornado.ioloop
import tornado.web

from handlers.array_handler import ArrayHandler

port = os.getenv('SERVER_PORT')


def make_app():
    return tornado.web.Application([
        (r"/arrays", ArrayHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(port)
    tornado.ioloop.IOLoop.current().start()
