import os

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker


class DatabaseConnector:
    DATABASE_USER = os.getenv('POSTGRES_USER', 'guest')
    DATABASE_PASSWORD = os.getenv('POSTGRES_PASSWORD', 'guest')
    DATABASE_NAME = os.getenv('POSTGRES_DB', 'test')
    DATABASE_IN_PORT = os.getenv('POSTGRES_IN_PORT', 5445)

    DATABASE_URL = f"postgresql+asyncpg://{DATABASE_USER}:{DATABASE_PASSWORD}@" \
                   f"database:{DATABASE_IN_PORT}/{DATABASE_NAME}"

    @classmethod
    async def get_db_session(cls):
        engine = create_async_engine(cls.DATABASE_URL)

        async_session = sessionmaker(
            engine, expire_on_commit=False, class_=AsyncSession
        )

        return async_session
