from sqlalchemy import Integer


def is_valid_array(array: list[Integer]) -> bool:
    if type(array) is not list:
        return False

    # Check on empty array
    if not array:
        return False

    for item in array:
        if type(item) not in [int, float]:
            return False

    return True
