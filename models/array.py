from sqlalchemy import Integer, Column, JSON, DateTime
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class ArrayModel(Base):
    __tablename__ = 'arrays'
    id = Column(Integer, primary_key=True)
    array = Column(JSON)
    result_array = Column(JSON)
    date_of_creation = Column(DateTime)
